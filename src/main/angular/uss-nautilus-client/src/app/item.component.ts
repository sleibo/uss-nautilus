import { Component, OnChanges, ViewChild, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ItemService } from './item.service';
import { Item } from './item';
import 'rxjs/Rx';

@Component( {
    selector: 'app-item',
    template: `
    <md-toolbar>
        <span>{{item.hostName}} / {{item.checkName}}</span> 
        <md-icon *ngIf="mustSave">save</md-icon>
        <span class="fill-remaining-space"></span>
        <button md-icon-button color="accent" (click)="delete()" > <md-icon>delete</md-icon> </button>
    </md-toolbar>
    
  <form novalidate #form="ngForm">
 
    <md-select name="state" [(ngModel)]="item.state" placeholder="state">
        <md-option value="OK">OK</md-option>
        <md-option value="WARNING">WARNING</md-option>
        <md-option value="CRITICAL">CRITICAL</md-option>
        <md-option value="UNKNOWN">UNKNOWN</md-option>
        <md-option value="FLAPPING">FLAPPING</md-option>
    </md-select>
    <md-form-field>
        <input mdInput name="output" [(ngModel)]="item.output" placeholder="output">
    </md-form-field>
    <md-form-field>
        <textarea  mdInput name="longOutput" [(ngModel)]="item.longOutput" placeholder="longOutput"></textarea>
    </md-form-field>
    <md-form-field>
        <input mdInput name="time" [(ngModel)]="item.time" placeholder="time">
    </md-form-field>
    <md-form-field>
        <input mdInput name="cpuTime" [(ngModel)]="item.cpuTime" placeholder="cpu time">
    </md-form-field>
  </form>
<app-metrics [item]="item" (changeItem)="onChangeItem($event)" (mustSaveItem)="onMustSaveItem($event)" ></app-metrics>
  `
} )
export class ItemComponent implements OnInit {
    @ViewChild( 'form' ) form: FormControl;
    @Input() item: Item;
    @Output() askRefresh = new EventEmitter();
    mustSave: boolean;

    constructor( private itemService: ItemService ) {
        console.log( '[ItemComponent] constructor' );
        this.mustSave = false;
    }
    ngOnInit(): void {
        console.log( '[ItemComponent]  init[', this.item, ']' );
        this.form.valueChanges
            .debounceTime( 600 )
            .distinctUntilChanged()
            .subscribe( term => {
                if ( this.form.dirty && this.item.hostName ) {
                    console.log( '[ItemComponent]  saving item [', this.item, ']' );
                    this.itemService.saveItem( this.item ).then( item => {
                        this.item = item;
                        this.mustSave = false;
                    }
                    );

                }
            } );

        this.form.valueChanges
            .subscribe( term => {
                if ( this.form.dirty && this.item.hostName ) {
                    this.mustSave = true;
                }
            } );
    }

    onChangeItem( item: Item ): void {
        console.log( '[ItemComponent] onMustSave' )
        this.mustSave = true;
    }
    onMustSaveItem( item: Item ): void {
        console.log( '[ItemComponent] onMustSave' )
        this.itemService.saveItem( this.item ).then( item => {
            this.item = item;
            this.mustSave = false;
        }
        );
    }
    delete(): void {
        console.log( '[ItemComponent] delete' )
        this.itemService.delete_item( this.item.hostName, this.item.checkName ).then(() => {
            console.log( '[ItemComponent] delete done' );
            this.askRefresh.emit()
        }
        );
    }


}

