import { Metric } from './metric';

export class Item {
    id: number;
    hostName: string;
    checkName: string;
    state: string;
    lastCode: number;
    output: string;
    longOutput: string;
    time: number;
    cpuTime: number;
    metrics: Metric[];
    constructor( id: number, hostName: string, checkName: string, state: string ) {
        this.id = id;
        this.hostName = hostName;
        this.checkName = checkName;
        this.state = state;
    }

}
