import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'mapPipe'})
export class MapPipe implements PipeTransform {
  transform(map: any, args?: any[]): Object[] {
    const returnArray = [];

    for (const key in map) {
      if (map.hasOwnProperty(key)) {
        returnArray.push({
          key: key,
          val: map[key]
        });
      }
    }
    return returnArray;
  }
}
