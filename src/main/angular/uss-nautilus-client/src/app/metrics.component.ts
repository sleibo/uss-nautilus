import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Metric } from './metric';
import { Item } from './item';

@Component( {
    selector: 'app-metrics',
    template: `
<div class="metric-contener">
    <h2>
        Metrics
        <span class="fill-remaining-space"></span>
        <button md-icon-button color="accent" (click)="addMetric()" > <md-icon>add_circle_outline</md-icon> </button>
    </h2>
    <app-metric *ngFor="let metric of item.metrics" [metric]="metric" (removeMetric)="onRemoveMetric($event)" (changeMetric)="onChangeMetric()" (mustSave)="onMustSave()"></app-metric>
</div>
  `
} )
export class MetricsComponent {
    @Input() item: Item;
    @Output() changeItem = new EventEmitter<Item>();
    @Output() mustSaveItem = new EventEmitter<Item>();

    addMetric(): void {
        console.log( '[MetricsComponent] addMetric' )
        this.item.metrics.push( new Metric( "new metric" ) )
        this.changeItem.emit( this.item );
        this.mustSaveItem.emit( this.item );
    }

    onRemoveMetric( metric: Metric ) {
        console.log( '[MetricsComponent] removeMetric', metric )

        var index = this.item.metrics.indexOf( metric, 0 );
        if ( index > -1 ) {
            this.item.metrics.splice( index, 1 );
        }
        this.changeItem.emit( this.item );
        this.mustSaveItem.emit( this.item );
    }

    onChangeMetric() {
        console.log( '[MetricsComponent] onChangeMetric' )
        this.changeItem.emit( this.item );
    }
    onMustSave() {
        console.log( '[MetricsComponent] onMustSave' )
        this.mustSaveItem.emit( this.item );
    }



}

