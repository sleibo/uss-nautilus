import { Component } from '@angular/core';
import { OnInit, Input } from '@angular/core';
import { ItemService } from './item.service';
import { Item } from './item';
import 'hammerjs';

@Component( {
    selector: 'app-root',
    template: `
<div class="contener">
    <md-toolbar color="primary">
        <h1>Welcome to {{title}}!</h1>
        <span class="fill-remaining-space"></span>
        <button class='small-margin' md-mini-fab (click)="refresh()"> <md-icon>refresh</md-icon> </button>
        <button md-mini-fab (click)="clean()"> <md-icon>delete</md-icon> </button>
    </md-toolbar>
    <md-card class="item-info" *ngFor="let i of items | mapPipe">
        <md-card-title>{{i.key}} details!</md-card-title>
        <md-card-content>
            <div class='item-part' *ngFor="let item of i.val">
                <app-item (askRefresh)="refresh()" [item]="item"></app-item>
            </div>
        </md-card-content>
    </md-card>
</div>
      `
} )
export class AppComponent implements OnInit {
    title = 'USS-Nautilus';
    items: Map<string, Item[]> = new Map();

    constructor( private itemService: ItemService ) {
        console.log( '[AppComponent] constructor' );
    }
    ngOnInit(): void {
        console.log( '[AppComponent] ngOnInit' );
        this.itemService.getItems().then( items => {
            console.log( '[AppComponent] handleItemService items[', items, '] this [', this, ']' );
            this.items = items;
        } );
    }

    clean(): void {
        console.log( '[AppComponent] clean' );
        this.itemService.cleanItems();
        this.items = new Map();
    }
    refresh(): void {
        console.log( '[AppComponent] refresh' );
        this.itemService.getItems().then( items => {
            console.log( '[AppComponent] handleItemService items[', items, '] this [', this, ']' );
            this.items = items;
        } );
    }
}


