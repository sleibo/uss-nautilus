import { Injectable } from '@angular/core';
import { Headers, Http, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Item } from './item';

@Injectable()
export class ItemService {

    private URL_GET_ITEMS = '/get_items';
    private URL_SAVE_ITEM = '/save_item';
    private URL_CLEAN_ITEMS = '/clean_items';
    private URL_DELETE_ITEM = '/delete_item';

    //        private URL_GET_ITEMS = 'http://localhost:8080/get_items';
    //        private URL_SAVE_ITEM = 'http://localhost:8080/save_item';
    //        private URL_CLEAN_ITEMS = 'http://localhost:8080/clean_items';
    //        private URL_DELETE_ITEM = 'http://localhost:8080/delete_item';

    constructor( private http: Http ) { }

    getItems(): Promise<Map<string, Item[]>> {
        console.log( '[ItemService] getItems' );
        return this.http.get( this.URL_GET_ITEMS )
            .toPromise()
            .then( response => {
                return Promise.resolve( response.json() );
            } )
            .catch( this.handleError );
    }

    saveItem( item: Item ): Promise<Item> {
        console.log( '[ItemService] saving item ', item );
        return this.http.post( this.URL_SAVE_ITEM, item )
            .toPromise()
            .then( response => {
                return Promise.resolve( response.json() );
            } )
            .catch( this.handleError );
    }

    cleanItems(): Promise<any> {
        console.log( '[ItemService] clean items' );
        return this.http.get( this.URL_CLEAN_ITEMS )
            .toPromise()
            .then( response => {
                return Promise.resolve();
            } )
            .catch( this.handleError );
    }

    delete_item( hostName: string, checkName: string ): Promise<any> {
        console.log( '[ItemService] delete items' );
        let params = new URLSearchParams();
        params.append( "host_name", hostName );
        params.append( "check_name", checkName );

        return this.http.get( this.URL_DELETE_ITEM, { search: params } )
            .toPromise()
            .then( response => {
                return Promise.resolve();
            } )
            .catch( this.handleError );
    }

    private handleError( error: any ): Promise<any> {
        console.error( '[ItemService] An error occurred', error );
        return Promise.reject( error.message || error );
    }
}
