import { Component, Input, ViewChild, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Metric } from './metric';

@Component( {
    selector: 'app-metric',
    template: `
    <form novalidate #form="ngForm">
        <md-form-field>
            <input mdInput name="name" [(ngModel)]="metric.name" placeholder="name">
        </md-form-field>
        <md-select name="type" [(ngModel)]="metric.type" placeholder="type">
            <md-option value="SINUS">SINUS</md-option>
            <md-option value="TRIANGLE">TRIANGLE</md-option>
            <md-option value="RECT">RECT</md-option>
            <md-option value="SAW">SAW</md-option>
        </md-select>
        <md-form-field>
            <input mdInput name="unit" [(ngModel)]="metric.unit" placeholder="unit">
        </md-form-field>
        <md-form-field>
            <input mdInput name="warning" [(ngModel)]="metric.warning" placeholder="warning">
        </md-form-field>
        <md-form-field>
            <input mdInput name="critical" [(ngModel)]="metric.critical" placeholder="critical">
        </md-form-field>
        <md-form-field>
            <input mdInput name="min" [(ngModel)]="metric.min" placeholder="min">
        </md-form-field>
        <md-form-field>
            <input mdInput name="max" [(ngModel)]="metric.max" placeholder="max">
        </md-form-field>
        
        <button md-icon-button color="accent" (click)="onRemoveMetric()"> <md-icon>delete</md-icon> </button>
    </form>
  `
} )
export class MetricComponent implements OnInit {
    @ViewChild( 'form' ) form: FormControl;
    @Input() metric: Metric;
    @Output() removeMetric = new EventEmitter<Metric>();
    @Output() changeMetric = new EventEmitter();
    @Output() mustSave = new EventEmitter();

    ngOnInit(): void {
        console.log( '[MetricComponent] init [', this.metric, ']' );
        this.form.valueChanges
            .debounceTime( 2000 )
            .distinctUntilChanged()
            .subscribe( term => {
                if ( this.form.dirty && this.metric.name ) {
                    console.log( '[MetricComponent] metric has change', this.metric );
                    this.mustSave.emit();
                }
            } );
        this.form.valueChanges
            .subscribe( term => {
                if ( this.form.dirty ) {
                    this.changeMetric.emit();
                }
            } );
    }

    onRemoveMetric(): void {
        console.log( '[MetricComponent] emit removeMetric' )
        this.removeMetric.emit( this.metric );
    }
}
