import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdSelectModule, MdInputModule, MdCardModule, MdButtonModule, MdIconModule, MdToolbarModule } from '@angular/material';

import { AppComponent } from './app.component';
import { ItemComponent } from './item.component';
import { MetricsComponent } from './metrics.component';
import { MetricComponent } from './metric.component';
import { MapPipe } from './map-pipe';
import { ItemService } from './item.service';

@NgModule( {
    declarations: [
        AppComponent,
        ItemComponent,
        MetricsComponent,
        MetricComponent,
        MapPipe
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        BrowserAnimationsModule,
        MdSelectModule,
        MdInputModule,
        MdCardModule,
        MdButtonModule,
        MdIconModule,
        MdToolbarModule
    ],
    providers: [ItemService],
    bootstrap: [AppComponent]
} )
export class AppModule { }
