import { UssNautilusClientPage } from './app.po';

describe('uss-nautilus-client App', () => {
  let page: UssNautilusClientPage;

  beforeEach(() => {
    page = new UssNautilusClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
