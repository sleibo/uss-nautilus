package shinken.com.uss.nautilus;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface ItemRepository extends CrudRepository<Item, Long> {
	Item findOneByHostNameAndCheckNameAllIgnoringCase(String hostName, String checkName);

	@Transactional
	void deleteByHostNameAndCheckNameAllIgnoringCase(String hostName, String checkName);

	boolean existsByHostNameAndCheckNameAllIgnoringCase(String hostName, String checkName);
}
