package shinken.com.uss.nautilus;

public class FormItem {
	private Long	id;
	private String	state;
	private String	output;
	private String	longOutput;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLongOutput() {
		return longOutput;
	}

	public void setLongOutput(String longOutput) {
		this.longOutput = longOutput;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("//");
		sb.append(id);
		sb.append(" - ");
		sb.append(state);
		sb.append("{");
		sb.append(output);
		sb.append("}");
		sb.append("{");
		sb.append(longOutput);
		sb.append("}");
		sb.append("//");
		return sb.toString();
	}

}
