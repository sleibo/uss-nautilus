package shinken.com.uss.nautilus;

import java.util.ArrayList;
import java.util.List;

public class ResultResponse {
	private int				code;
	private String			output;
	private String			longOutput;
	private float			time;
	private float			cpuTime;
	private List<Metric>	metrics	= new ArrayList<>();

	public ResultResponse() {
	}

	public ResultResponse(int code, String output, String longOutput, float time, float cpuTime, List<Metric> metrics) {
		super();
		this.code = code;
		this.output = output;
		this.longOutput = longOutput;
		this.metrics = metrics;
		this.time = time;
		this.cpuTime = cpuTime;
	}

	public float getTime() {
		return time;
	}

	public void setTime(float time) {
		this.time = time;
	}

	public float getCpuTime() {
		return cpuTime;
	}

	public void setCpuTime(float cpuTime) {
		this.cpuTime = cpuTime;
	}

	public List<Metric> getMetrics() {
		return metrics;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getLongOutput() {
		return longOutput;
	}

	public void setLongOutput(String longOutput) {
		this.longOutput = longOutput;
	}

}
