package shinken.com.uss.nautilus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainPageController {
	private static final Logger	LOG	= LogManager.getLogger(MainPageController.class);

	@Autowired
	private ItemRepository		itemRepository;

	@RequestMapping(value = "/old", method = RequestMethod.GET)
	public String mainPage(Model model) {
		Map<String, List<Item>> itemsByHost = new TreeMap<>();
		Iterable<Item> items = itemRepository.findAll();

		for (Item item : items) {
			String hostName = item.getHostName();
			List<Item> hostItems = itemsByHost.getOrDefault(hostName, new ArrayList<>());

			if (StringUtils.isEmpty(item.getCheckName())) {
				hostItems.add(0, item);
			}
			else {
				hostItems.add(item);
			}
			itemsByHost.put(hostName, hostItems);
		}

		model.addAttribute("itemsByHost", itemsByHost);
		model.addAttribute("state", Item.State.CRITICAL.name());
		return "mainPage";
	}

	@RequestMapping(value = "/old", method = RequestMethod.POST)
	public String save(FormItem formItem, Model model) {

		LOG.debug("saving item : [{}]", formItem);

		Item itemToUpdate = itemRepository.findOne(formItem.getId());

		itemToUpdate.setState(Item.State.valueOf(formItem.getState()));
		itemToUpdate.setOutput(formItem.getOutput());
		itemToUpdate.setLongOutput(formItem.getLongOutput());

		itemRepository.save(itemToUpdate);
		return mainPage(model);
	}

	@RequestMapping(value = "/cleanAll", method = RequestMethod.GET)
	public String cleanAll(Model model) {
		itemRepository.deleteAll();
		return mainPage(model);
	}
}