package shinken.com.uss.nautilus;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Metric {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long		id;

	private String		name;
	private String		unit;
	private String		warning;
	private String		critical;
	private String		min;
	private String		max;
	private Double		value;
	private int			periode	= 5 * 60;
	private MetricType	type;

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("M[") //
		        .append(id).append("-").append(name)//
		        .append(" (type:").append(type)//
		        .append(" value:").append(value)//
		        .append(" unit:").append(unit)//
		        .append(" war:").append(warning)//
		        .append(" crit:").append(critical)//
		        .append(" min:").append(min)//
		        .append(" max:").append(max)//
		        .append("]");//
		return sb.toString();
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getWarning() {
		return warning;
	}

	public void setWarning(String warning) {
		this.warning = warning;
	}

	public String getCritical() {
		return critical;
	}

	public void setCritical(String critical) {
		this.critical = critical;
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	public MetricType getType() {
		return type;
	}

	public void setType(MetricType type) {
		this.type = type;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public void nextValue() {
		long counter = (System.currentTimeMillis() / 1000) % periode;
		double midPeriode = periode / 2;
		switch (type) {
			case SINUS:
				setValue(Math.sin((2d * Math.PI * counter) / (double) periode));
			break;
			case TRIANGLE:
				setValue((double) counter);
			break;
			case RECT:
				if (counter > midPeriode) {
					setValue(midPeriode);
				}
				else {
					setValue(0d);
				}
			break;
			case SAW:
				if (counter > midPeriode) {
					setValue((double) periode - counter);
				}
				else {
					setValue((double) counter);
				}
			break;

			default:
				setValue(0d);
			break;
		}
	}

	enum MetricType {
		SINUS, TRIANGLE, RECT, SAW
	}

}
