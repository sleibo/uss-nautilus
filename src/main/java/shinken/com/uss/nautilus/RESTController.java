package shinken.com.uss.nautilus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RESTController {
	private static final Logger	LOG	= LogManager.getLogger(RESTController.class);

	@Autowired
	private ItemRepository		itemRepository;
	@Autowired
	private MetricRepository	metricRepository;

	@CrossOrigin(origins = "*")
	@RequestMapping("/get_result")
	public ResultResponse getResult(@RequestParam(value = "host_name") String hostName, @RequestParam(value = "check_name", required = false, defaultValue = "") String checkName) {
		LOG.debug("get_result [{}-{}]", hostName, checkName);
		if (!itemRepository.existsByHostNameAndCheckNameAllIgnoringCase(hostName, checkName)) {
			itemRepository.save(new Item(hostName, checkName));
		}

		Item item = itemRepository.findOneByHostNameAndCheckNameAllIgnoringCase(hostName, checkName);

		int code;
		code = getStateCode(item);
		for (Metric metric : item.getMetrics()) {
			metric.nextValue();
			metricRepository.save(metric);
		}
		itemRepository.save(item);
		return new ResultResponse(code, item.getOutput(), item.getLongOutput(), item.getTime(), item.getCpuTime(), item.getMetrics());
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/save_item", method = RequestMethod.POST)
	public Item save(@RequestBody Item item) {
		LOG.debug("saving item : [{}]", item);
		if (!StringUtils.isEmpty(item.getHostName())) {
			for (Metric metric : item.getMetrics()) {
				metricRepository.save(metric);
			}
			itemRepository.save(item);
		}

		LOG.debug("saving item save: [{}]", item);

		return item;
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/delete_item", method = RequestMethod.GET)
	public void deleteItem(@RequestParam(value = "host_name") String hostName, @RequestParam(value = "check_name", required = false, defaultValue = "") String checkName) {
		LOG.debug("delete item [{}-{}]", hostName, checkName);
		itemRepository.deleteByHostNameAndCheckNameAllIgnoringCase(hostName, checkName);
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/clean_items", method = RequestMethod.GET)
	public void clean() {
		LOG.debug("clean all item");
		itemRepository.deleteAll();
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/get_items", method = RequestMethod.GET)
	public Map<String, List<Item>> getItems(Model model) {
		Map<String, List<Item>> itemsByHost = new TreeMap<>();
		Iterable<Item> items = itemRepository.findAll();
		List<Item> tmpRet = new ArrayList<>();

		for (Item item : items) {
			tmpRet.add(item);
			String hostName = item.getHostName();
			List<Item> hostItems = itemsByHost.getOrDefault(hostName, new ArrayList<>());

			if (StringUtils.isEmpty(item.getCheckName())) {
				hostItems.add(0, item);
			}
			else {
				hostItems.add(item);
			}
			itemsByHost.put(hostName, hostItems);
		}

		return itemsByHost;
	}

	private int getStateCode(Item item) {
		int code;
		switch (item.getState()) {
			case FLAPPING:
				code = item.getLastCode();
				if (code == State.OK.getCode()) {
					code = State.CRITICAL.getCode();
				}
				else {
					code = State.OK.getCode();
				}
				item.setLastCode(code);
			break;
			case UNKNOWN:
				code = State.UNKNOWN.getCode();
			break;
			case CRITICAL:
				code = State.CRITICAL.getCode();
			break;
			case WARNING:
				code = State.WARNING.getCode();
			break;
			case OK:
			default:
				code = State.OK.getCode();
			break;
		}
		return code;
	}

}